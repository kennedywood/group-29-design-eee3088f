# Getting Started with the Temperature & Humidity Sensor

## Description
The Temperature & Humidity Sensor is a "Hardware Attached on Top" (HAT) which extends the STM32F0 Discovery Board so that it can read room temperature and humidity values to a computer. The HAT uses both a digital and analog sensor and uses an 18650 cell for portable use.

### Features:
- Li-Ion Battery Charger
- Battery Polarity Protection
- Battery Under-Voltage Cutout Protection

## What Hardware you will need
- STM32F0 Discovery Board
- USB cable Type-A to Mini-B to power the discovery board
- USB cable Type-A to micro USB to communicate to the HAT

## What Software/Tools you will need
STM32CubeIDE is needed to connect to the HAT via the micro USB to a computer to communicate with it.

## How to connect the hardware
Place the HAT directly on the Discovery Board with both USB ports aligning and such that the pins of the Discovery Board go through the the pin ports on the HAT.
Connecting the Discovery Board to power will charge the battery if it is dead and powers the HAT regardless if the cell is charged or not, else the HAT can be powered through the cell.
To communicate with the HAT connect the respective USB cable to the HAT port and to a computer and use STM32CubeIDE.

## Support help
Email one of the following people for help with the respective elements of the HAT:
Sensing - Kimmy Sithole: STHKIM002@myuct.ac.za
Microcontroller Interfacing - Karan Abraham: ABRKAR004@myuct.ac.za
Power Management - Kennedy Wood: WDXKEN001@myuct.ac.za

## License
MIT License

Copyright (c) [2022] [Kimmy Sithole, Karan Abraham, Kennedy Wood]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.