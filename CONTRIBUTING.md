Engineer should submit a pull request on GitLab. Once granted, he should push his changes to a new branch. The changes will be reviewed and if approved then merged to the main branch.

Issues should be brought up in the “issues” tab on GitLab. KiCad needs to be used to edit the PCB files. STM32CubeIDE is recommended for the code.
